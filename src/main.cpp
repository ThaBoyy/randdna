#include <iostream>
#include <random>
#include <string>
#include <vector>

using namespace std;

string randDNA(int s, string b, int n)
{
    mt19937 eng(s);
    uniform_int_distribution<int> uniform(0, b.size() - 1);

    string ans;

    for (int l = 0; l < n; l++)
        {
            ans = ans + b[uniform(eng)];
        }

    return ans;
}
